# Hello, all! <img src="https://github.com/JoshyB14/JoshyB14/blob/main/wave.gif" width="30px">
Hi, I'm Josh - a Data Engineer at EY! Previously MSc @ UNSW and BMedSci @ USYD.
  
<img align="right" alt="GIF" src="https://github.com/JoshyB14/JoshyB14/blob/main/code.gif?raw=true" width="500" height="320" />

# 🔧 Technologies & Tools

![SQL](https://img.shields.io/badge/-SQL-05122A?style=for-the-badge&logo=sql)&nbsp;
![PYTHON](https://img.shields.io/badge/-Python-05122A?style=for-the-badge&logo=python)&nbsp;
![GIT](https://img.shields.io/badge/-Git-05122A?style=for-the-badge&logo=git)&nbsp;
![DBT](https://img.shields.io/badge/-DBT-05122A?style=for-the-badge&logo=dbt)&nbsp;
![Postgres](https://img.shields.io/badge/-postgres-05122A?style=for-the-badge&logo=postgresql)&nbsp;
![DUCKDB](https://img.shields.io/badge/-DuckDB-05122A?style=for-the-badge&logo=duckdb)&nbsp;
![SNOWFLAKE](https://img.shields.io/badge/-Snowflake-05122A?style=for-the-badge&logo=snowflake)&nbsp;
![GitHub](https://img.shields.io/badge/-GitHub-05122A?style=for-the-badge&logo=github)&nbsp;
![GitLab](https://img.shields.io/badge/gitlab-05122A?style=for-the-badge&logo=gitlab&logoColorr=007ACC)&nbsp;
![Sublime Text](https://img.shields.io/badge/sublime_text-05122A?style=for-the-badge&logo=sublime-text&logoColor=007ACC)&nbsp;
![Visual Studio Code](https://img.shields.io/badge/-Visual%20Studio%20Code-05122A?style=for-the-badge&logo=visual-studio-code&logoColor=007ACC)&nbsp;
![Azure](https://img.shields.io/badge/-azure-05122A?style=for-the-badge&logo=microsoftazure)&nbsp;
![AWS](https://img.shields.io/badge/-AWS-05122A?style=for-the-badge&logo=amazon-aws)&nbsp;


# 🧙‍♂️ Projects 


### Modern Data Stack In A Box :floppy_disk:
Showcasing the modern data stack on a local machine. Utilising dbt and DuckDB to analyse F1 data. [See here](https://github.com/JoshyB14/duckdb_f1)

### Advent of Code 2022 :christmas_tree: 
Attempting the 2022 Advent of Code using DuckDB and dbt. [See here](https://github.com/JoshyB14/advent_of_code_2022)



